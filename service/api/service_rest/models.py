from django.db import models
from django.urls import reverse
# Create your models here.


class Technician(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    employee_id = models.PositiveIntegerField(unique=True)

    def get_api_url(self):
        return reverse('technicial_detail', kwargs={"pk": self.id})


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=30, unique=True, null=True)
    sold = models.BooleanField(default=False)



class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=500)
    vin = models.CharField(max_length=30)
    customer = models.CharField(max_length=50)
    status = models.CharField(max_length=50, default="pending")
    vip = models.BooleanField(default=False)
    is_finished = models.BooleanField(default=False)
    technician = models.ForeignKey(
        Technician,
        related_name = "appointments",
        on_delete=models.PROTECT
    )

    def get_api_url(self):
        return reverse('show_appointment', kwargs={"pk": self.id})
