from django.shortcuts import render
from .models import AutomobileVO, Technician, Appointment
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
# Create your views here.

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = ["first_name",
                  "last_name",
                  "employee_id",
                  "id",
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date_time",
        "reason",
        "vin",
        "customer",
        "status",
        "vip",
        "is_finished",
        "technician",
    ]

    encoders = {
        "technician": TechnicianEncoder(),
    }

#APPOINTMENT VIEWS

@require_http_methods(["GET", "POST"])
def list_appointments(request, vin=None):
    if request.method == "GET":
        if vin == None:
            appointments = Appointment.objects.all()
            return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder
        )
        else:
            try:
                appointments = Appointment.objects.filter(vin=vin)
                return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder
        )
            except Appointment.DoesNotExist:
                return JsonResponse({"message": "Appointment does not exist"},
                        status=400)

    #POST
    else:
        content = json.loads(request.body)
        try:
            technician_id = content["technician"]
            technician = Technician.objects.get(id=technician_id)
            content["technician"] = technician

        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "This technician does not exist"},
                status=400
            )

    #checks to see if inputted vin is in our database and set to VIP if it exists.

        if AutomobileVO.objects.filter(vin=content["vin"]).exists():
            content["vip"] = True
        else:
            content["vip"] = False

        appointment = Appointment.objects.create(**content)

        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def appointment_detail(request, pk):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False
            )

        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Appointment does not exist"},
                        status=400)

    elif request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment.delete()
            return JsonResponse(
                {"message": "Appointment deleted successfully."}
            )

        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Appointment does not exist"},
                        status=400)

    else:
        content = json.loads(request.body)
        Appointment.objects.filter(id=pk).update(**content)
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )


#SEPARATE VIEW FUNCTION TO SET STATUS TO "CANCELLED"
@require_http_methods(["PUT"])
def cancel_appointment(request, pk):
    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=pk)
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Appointment does not exist"}, status=404)


        appointment.status = "canceled"
        appointment.save()

        return JsonResponse(
            {"message": "canceled", "status": "canceled"},
            status=200
        )

#SEPARATE VIEW FUNCTION TO SET STATUS TO "FINISHED"
@require_http_methods(["PUT"])
def finish_appointment(request, pk):
    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=pk)
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Appointment does not exist"}, status=404)


        appointment.status = "finished"
        appointment.save()

        return JsonResponse(
            {"message": "finished", "status": "finished"},
            status=200
        )

#TECHNICIAN VIEWS

@require_http_methods(["GET", "POST"])
def list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()

        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder
        )

    # POST
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)

        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def technician_detail(request, pk):

    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False
            )

        except Technician.DoesNotExist:
            return JsonResponse({"message": "Technician not found"},
                        status=400)


    # PUT
    elif request.method == "PUT":
        content = json.loads(request.body)
        Technician.objects.filter(id=pk).update(**content)
        technician = Technician.objects.get(id=pk)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False
        )

    #DELETE
    else:
        try:
            technician = Technician.objects.get(id=pk)
            technician.delete()
            return JsonResponse(
                {"message": "Technician deleted successfully"}
            )

        except Technician.DoesNotExist:
            return JsonResponse({"message": "Technician not found"},
                     status=400)
