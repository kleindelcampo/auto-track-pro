from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import AutomobileVO, Sale, Salesperson, Customer
from common.json import ModelEncoder
from django.http import JsonResponse
import json
# Create your views here.



class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "id",
        "vin",
        "sold",
    ]



class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id"
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "first_name",
        "last_name",
        "address",
        "phone_number"
    ]

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "price",
        "automobile",
        "salesperson",
        "customer",
    ]

    encoders = {
        "automobile": AutomobileVOEncoder(),
        "salesperson": SalespersonEncoder(),
        "customer": CustomerEncoder(),
        }

class SaleDetailEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "price",
        "automobile",
        "salesperson",
    ]

    def get_extra_data(self, o):
        return {"automobile": { "id" : o.automobile.id,
                                "sold" : o.automobile.sold},
                "salesperson":{"first_name": o.salesperson.first_name,
                               "last_name": o.salesperson.last_name,
                               "employee_id": o.salesperson.employee_id},
                "customer": {"first_name": o.customer.first_name,
                             "last_name": o.customer.last_name,
                             "phone_number": o.customer.phone_number} }



@require_http_methods(["GET", "POST"])
def api_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()

        return JsonResponse({"salespeople": salespeople}, encoder=SalespersonEncoder, safe=False)

    else:
        try:
            content = json.loads(request.body)
            salespeople = Salesperson.objects.create(**content)
            return JsonResponse(salespeople, encoder=SalespersonEncoder, safe=False)

        except:
            response = JsonResponse({"message": "Could not create salesperson."})
            response.status_code = 400
            return response



@require_http_methods(["GET", "DELETE"])
def api_salesperson(request, pk):
    if request.method == "GET":
        try:
            salesperson = Salesperson.objects.get(id=pk)
            return JsonResponse(salesperson, encoder=SalespersonEncoder, safe=False)

        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

    elif request.method == "DELETE":
        try:
            salesperson = Salesperson.objects.get(id=pk)
            salesperson.delete()
            return JsonResponse(salesperson, encoder=SalespersonEncoder, safe=False)

        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response



@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        print(customers)
        return JsonResponse({"customers": customers}, encoder=CustomerEncoder, safe=False)

    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(customer, encoder=CustomerEncoder, safe=False)

        except:
            response = JsonResponse({"message": "Could not create customer."})
            response.status_code = 400
            return response



@require_http_methods(["GET", "DELETE"])
def api_customer(request, pk):
    if request.method =="GET":
        try:
            customer = Customer.objects.get(id=pk)
            return JsonResponse(customer, encoder=CustomerEncoder, safe=False)

        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

    elif request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=pk)
            customer.delete()
            return JsonResponse(customer, encoder=CustomerEncoder, safe=False)

        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response



@require_http_methods(["GET", "POST"])
def api_sales(request):
    if request.method =="GET":
        sales = Sale.objects.all()
        print(sales)
        return JsonResponse({"sales": sales}, encoder=SaleEncoder, safe=False)

    else:
        try:
            content = json.loads(request.body)
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            salesperson = Salesperson.objects.get(id=content["salesperson"])
            customer = Customer.objects.get(id=content["customer"])
            content["automobile"] = automobile
            content["salesperson"] = salesperson
            content["customer"] = customer

        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile id"},
                status=400,
            )

        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid salesperson id"},
                status=400,
            )

        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer id"},
                status=400,
            )

        sale = Sale.objects.create(**content)
        return JsonResponse(sale, encoder=SaleEncoder, safe=False)



@require_http_methods(["GET", "DELETE", "PUT"])
def api_sale(request, pk):
    if request.method == "GET":
        try:
            sale = Sale.objects.get(id=pk)
            return JsonResponse(sale, encoder=SaleDetailEncoder, safe=False)

        except Sale.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

    elif request.method == "DELETE":
        try:
            sale = Sale.objects.get(id=pk)
            sale.delete()
            return JsonResponse(sale, encoder=SaleEncoder, safe=False)

        except Sale.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            content = json.loads(request.body)
            sale = Sale.objects.filter(id=pk).update(**content)
            return JsonResponse(sale, encoder=SaleDetailEncoder, safe=False)

        except Sale.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
