import React, {useState, useEffect} from 'react';

function SalesList() {
    const [sales, setSales] = useState([])
    async function getSales() {
        const response = await fetch('http://localhost:8090/api/sales/')
        if (response.ok) {
            const {sales} = await response.json();
            setSales(sales)




        };

    }

    useEffect(() => {
        getSales();
    }, []);



    return (
        <>
        <div className="px-4 py-5 my-1 mt-0 text-center">
        <h1 className="display-5">Sales</h1>
        <table  className="table table-striped">
            <thead>
                <tr className="table-secondary">
                    <th>Employee ID</th>
                    <th>Salesperson Name</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Price</th>
                    <th>Sold</th>
                </tr>
            </thead>
            <tbody>
                {sales.map(sale =>{
                    return(
                        <tr key={sale.id}>
                            <td>{sale.salesperson.employee_id}</td>
                            <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                            <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>{sale.price}</td>
                            <td>{sale.automobile.sold ? "Yes" : "No"} </td>
                        </tr>
                    )
                })}

            </tbody>
        </table>
        </div>
        </>
    );
}

export default SalesList;
