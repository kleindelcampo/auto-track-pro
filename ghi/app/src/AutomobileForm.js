import React, {useState, useEffect} from 'react';


function AutomobileForm() {
    const [color, setColor] = useState('');
    const [year, setYear] = useState('');
    const [vin, setVin] = useState('');
    const [models, setModels] = useState([]);
    const [model, setModel] = useState('');

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setModels(data.models)
        }

    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleColorChange = (e) => {
        const value = e.target.value;
        setColor(value);
    }

    const handleYearChange = (e) => {
        const value = e.target.value;
        setYear(value);
    }

    const handleVinChange = (e) => {
        const value = e.target.value;
        setVin(value);
    }

    const handleModelChange = (e) => {
        const value = e.target.value;
        setModel(value);
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        const data = {};

        data.color = color;
        data.year = year;
        data.vin = vin;
        data.model_id = model;

        const autoUrl = 'http://localhost:8100/api/automobiles/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type" : "application.json"
            },
        };

        const response = await fetch(autoUrl, fetchConfig);
        if (response.ok) {
            const newAuto = await response.json();

            setColor('');
            setYear('');
            setVin('');
            setModel('');
        }


    };


    return (
        <div className="container">
            <div className="col">
                <form onSubmit={handleSubmit} id="create-auto-form">
                    <h1 className='card-title'>Add an automobile to inventory</h1>
                    <div className="row">
                        <div className="col">
                            <div className="form-floating mb-3">
                            <input onChange={handleColorChange} value={color} placeholder="Color" type="text" id="color" name="color" className="form-control" />
                            <label htmlFor="color">Color...</label>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col">
                            <div className="form-floating mb-3">
                            <input onChange={handleYearChange} value={year} placeholder="Year" type="number" id="year" name="year" className="form-control" />
                            <label htmlFor="year">Year...</label>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col">
                            <div className="form-floating mb-3">
                            <input onChange={handleVinChange} value={vin} placeholder="VIN" type="text" id="vin" name="vin" className="form-control" />
                            <label htmlFor="vin">VIN...</label>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col">
                            <div className="form-floating mb-3">
                            <select onChange={handleModelChange} value={model} name="model" id="model" className="form-select" required>
                            <option value="">Choose a model...</option>
                            {models.map(model => {
                                return (
                                    <option key={model.id} value={model.id}>{model.name}</option>
                                )
                            })}
                            </select>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="mb-3">
                            <button className="btn btn-lg btn-primary">Create</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default AutomobileForm;
