import React, {useState, useEffect} from 'react';

function CustomerList() {
    const [customers, setCustomers] = useState([]);
    async function getCustomers() {
        const response = await fetch('http://localhost:8090/api/customers/');
        if (response.ok) {
            const {customers} = await response.json();
            setCustomers(customers)
        } else {
            console.error("An error occured while fetching this data.");
        };
    }

    useEffect(() => {
        getCustomers();
    }, [])

    return (
        <>
        <div className="px-4 py-5 my-1 mt-0 text-center">
        <h1 className="display-5">Customer List</h1>
        <table className='table table-striped'>
            <thead>
                <tr className='table-secondary'>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Address</th>
                    <th>Phone #</th>
                </tr>
            </thead>
            <tbody>
                {customers.map(customer =>{
                    return (
                        <tr key={(customer.phone_number)}>
                            <td>{customer.first_name}</td>
                            <td>{customer.last_name}</td>
                            <td>{customer.address}</td>
                            <td>{customer.phone_number}</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
        </div>
        </>
    );
}

export default CustomerList;
