import { useEffect, useState } from "react";

const AppointmentForm = () => {
    const [dateTime, setDateTime] = useState("");
    const [reason, setReason] = useState("");
	  const [vin, setVin] = useState("");
	  const [customer, setCustomer] = useState("");
	  const [technicians, setTechnicians] = useState([]);
	  const [technician, setTechnician] = useState("");


    const handleDateTimeChange = (e) => {
        const value = e.target.value;
        setDateTime(value);
    }

    const handleReasonChange = (e) => {
        const value = e.target.value;
        setReason(value);
    }

    const handleVinChange = (e) => {
        const value = e.target.value;
        setVin(value);
    }

    const handleCustomerChange = (e) => {
        const value = e.target.value;
        setCustomer(value);
    }

    const handleTechnicianChange = (e) => {
      const value = e.target.value;
      setTechnician(value);
    };

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/';
        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setTechnicians(data.technicians);
            }
        } catch (err) {
            console.error(err);
        }
    };


    const handleSubmit = async (e) => {
        e.preventDefault();

        const appointmentUrl = "http://localhost:8080/api/appointments/";
        const data = {
            date_time: dateTime,
            reason: reason,
            vin: vin,
            customer: customer,
            technician: technician,
        };

        const fetchOptions = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        };

        try {
            const response = await fetch(appointmentUrl, fetchOptions);
            if (response.ok) {
                setDateTime('');
                setReason('');
                setVin('');
                setCustomer('');
                setTechnician('');

                fetchData();
            } else {
                console.error('Failed to create appointment');
            }
        } catch (error) {
            console.error('Error:', error);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
          <div className="offset-md-3 col-md-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a Service Appointment</h1>
                        <form onSubmit={handleSubmit} id="create-appointment-form">
                <div className="form-floating mb-3">
                  <input
                    onChange={handleDateTimeChange}
                    value={dateTime}
                    required
                    type="datetime-local"
                    name="datetime"
                    id="datetime"
                    className="form-control"
                  />
                  <label htmlFor="datetime">Choose Date and Time</label>
                </div>

                <div className="form-floating mb-3">
                  <input
                    onChange={handleReasonChange}
                    placeholder="Reason"
                    value={reason}
                    required
                    type="text"
                    name="reason"
                    id="reason"
                    className="form-control"
                  />
                  <label htmlFor="reason">Reason:</label>
                </div>

                <div className="form-floating mb-3">
                  <input
                    onChange={handleVinChange}
                    placeholder="VIN"
                    value={vin}
                    required
                    type="text"
                    name="vin"
                    id="vin"
                    className="form-control"
                  />
                  <label htmlFor="vin">VIN</label>
                </div>

                <div className="form-floating mb-3">
                  <input
                    onChange={handleCustomerChange}
                    placeholder="Customer"
                    value={customer}
                    required
                    type="text"
                    name="customer"
                    id="customer"
                    className="form-control"
                  />
                  <label htmlFor="customer">Owner</label>
                </div>

                <div className="mb-3">
                  <select value={technician}
                    onChange={handleTechnicianChange}
                    required id="technician"
                    name="technician"
                    className="form-select"
                  >
                    <option value="">Choose a technician</option>
                    {technicians.map((technician) => (
                    <option value={technician.id} key={technician.id}>
                    {technician.first_name}
                      </option>
                    ))}
                  </select>
                </div>
                <button className="btn btn-primary">Create Appointment</button>
              </form>
            </div>
          </div>
        </div>

      );
    };

export default AppointmentForm;
