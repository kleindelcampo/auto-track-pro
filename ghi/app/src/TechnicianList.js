import { useEffect, useState } from "react";

const TechnicianList = () => {
	const [technicianList, setTechnicianList] = useState([]);

	useEffect(() => {
		async function fetchTechnicianList() {
			const url = "http://localhost:8080/api/technicians/";
			const response = await fetch(url);

			if (response.ok) {
				const data = await response.json();
				setTechnicianList(data.technicians);
			}
		}
		fetchTechnicianList();
	}, []);

	return (
        <div className="px-4 py-5 my-1 mt-0 text-center">
				<h1 className="display-5">Technician List</h1>
		<table className="table table-striped mt-4 table-hover">
			<thead>
				<tr>
                    <th>Employee ID</th>
					<th>First Name</th>
					<th>Last Name</th>
				</tr>
			</thead>
			<tbody>
				{technicianList.map((technician) => {
					return (
						<tr className="table-row" key={technician.employee_id}>
							<td>{technician.employee_id}</td>
							<td>{technician.first_name}</td>
							<td>{technician.last_name}</td>
						</tr>
					);
				})}
			</tbody>
		</table>
        </div>
	);
};
export default TechnicianList;
