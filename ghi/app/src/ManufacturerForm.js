import React, {useEffect, useState} from 'react'

function ManufacturerForm(props) {


const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.name = name;
    const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };


    const response = await fetch(manufacturerUrl, fetchConfig);
    if (response.ok) {
      const newManufacturer = await response.json();
      setName("");
      props.fetchManufacturers();
    }
  }

const [name, setName] = useState('');
    const handleNameChange = (e) => {
      const value = e.target.value;
      setName(value);
    }


  return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a manufacturer!</h1>
            <form onSubmit={handleSubmit} id="create-manufacturer-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="manufacturer" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <button className="btn btn-primary">Create Manufacturer</button>
            </form>
          </div>
        </div>
      </div>
  )
}


export default ManufacturerForm
