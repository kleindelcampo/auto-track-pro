import React, {useState, useEffect} from 'react';

function SalesForm() {
    const [autos, setAutos] = useState([]);
    const [auto, setAuto] = useState('');
    const [salespeople, setSalespeople] = useState([]);
    const [salesperson, setSalesperson] = useState('');
    const [customers, setCustomers] = useState([]);
    const [customer, setCustomer] = useState('');
    const [price, setPrice] = useState('');
    const [sold, setSold] = useState(true);


    const fetchSalespeople = async () => {
        const url = "http://localhost:8090/api/salespeople/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople)

        }

    }

    useEffect(() => {
        fetchSalespeople();
    }, []);

    const fetchCustomers = async () => {
        const url = 'http://localhost:8090/api/customers/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers)
        }
    }

    useEffect(() => {
        fetchCustomers();
    }, []);


    const fetchAutos = async () => {
        const url = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setAutos(data.autos)
        }

    }

    useEffect(() => {
        fetchAutos();
    }, []);

    const handleAutoChange = (e) => {
        const value = e.target.value;
        setAuto(value)
    }

    const handleSalespersonChange = (e) => {
        const value = e.target.value;
        setSalesperson(value)
    }

    const handleCustomerChange = (e) => {
        const value = e.target.value;
        setCustomer(value)
    }

    const handlePriceChange = (e) => {
        const value = e.target.value;
        setPrice(value)
    }

    const handleSubmit = async (e) => {
        e.preventDefault();


        const data = {}

        data.price = price;
        data.automobile = auto;
        data.salesperson = salesperson;
        data.customer = customer;

        const saleUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application.json',
            },
        };

        const response = await fetch(saleUrl, fetchConfig);

        if (response.ok) {
            const newSale = await response.json();

            setAuto('');
            setSalesperson('');
            setCustomer('');
            setPrice('');

        }
    }

    return (
        <div className="row">
			<div className="offset-3 col-6">
				<div className="px-4 py-5 my-1 mt-0 text-center">
                 <form onSubmit={handleSubmit} id="create-sale-form">
                    <h1 className='display-5'>Create new Sale</h1>
                    <div className="row">
                        <div className="col">
                            <div className="form-floating mb-3">
                            <select  onChange={handleAutoChange} value={auto} name="Vin" id="vin" className="form-select" required>
                            <option value="">Choose Automobile VIN</option>
                                {autos.map(auto => {
                                    return (
                                        <option key={auto.id} value={auto.vin}>{auto.vin}</option>
                                    )
                                })}
                            </select>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col">
                            <div className="form-floating mb-3">
                            <select onChange={handleSalespersonChange} value={salesperson} name="salesperson" id="salesperson" className="form-select" required>
                            <option value="">Choose a Salesperson</option>
                                {salespeople.map(salesperson => {
                                    return (
                                        <option key={salesperson.id} value={salesperson.id}>{salesperson.first_name} {salesperson.last_name}</option>
                                    )
                                })}
                            </select>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col">
                            <div className="form-floating mb-3">
                            <select onChange={handleCustomerChange} value={customer} name="customer" id="customer" className="form-select" required>
                            <option value="">Choose a Customer</option>
                                {customers.map(customer => {
                                    return (
                                        <option key={customer.id} value={customer.id}>{customer.first_name} {customer.last_name}</option>
                                    )
                                })}
                            </select>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                            <div className="col">
                                <div className="form-floating mb-3">
                                <input onChange={handlePriceChange} value={price} required placeholder="Price" type="text" id="price" name="price" className="form-control" />
                                <label htmlFor="name">Price</label>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="mb-3">
                                <button className="btn btn-lg btn-primary">Create Sale</button>
                            </div>
                        </div>
                 </form>
                </div>
            </div>
        </div>
    );
}

export default SalesForm;
