import React, {useState, useEffect} from 'react';


function CustomerForm() {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [address, setAddress] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');

    const handleFirstNameChange = (e) => {
        const value = e.target.value;
        setFirstName(value);
    }

    const handleLastNameChange = (e) => {
        const value = e.target.value;
        setLastName(value);
    }

    const handleAddressChange = (e) => {
        const value = e.target.value;
        setAddress(value);
    }

    const handlePhoneNumberChange = (e) => {
        const value = e.target.value;
        setPhoneNumber(value);
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        const data = {};

        data.first_name = firstName;
        data.last_name = lastName;
        data.address = address;
        data.phone_number = phoneNumber;

        const url = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application.json',
                },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            const newCustomer = await response.json();

            setFirstName('');
            setLastName('');
            setAddress('');
            setPhoneNumber('');
        } else {
            alert("There is already someone registered with that phone number.");
        }

    }


    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="px-4 py-5 my-1 mt-0 text-center">
                    <form onSubmit={handleSubmit} id="create-customer-form">
                        <h1 className="display-5">Create New Customer</h1>
                        <div className="row">
                            <div className="col">
                                <div className="form-floating mb-3">
                                <input onChange={handleFirstNameChange} value={firstName} required placeholder="First name" type="text" id="firstName" name="firstName" className="form-control" />
                                <label htmlFor="firstName">First name</label>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <div className="form-floating mb-3">
                                <input onChange={handleLastNameChange} value={lastName} required placeholder="Last name" type="text" id="lastName" name="lastName" className="form-control" />
                                <label htmlFor="lastName">Last name</label>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <div className="form-floating mb-3">
                                <input onChange={handleAddressChange} value={address} required placeholder="Address" type="text" id="address" name="address" className="form-control" />
                                <label htmlFor="address">Address</label>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <div className="form-floating mb-3">
                                <input onChange={handlePhoneNumberChange} value={phoneNumber} required placeholder="phone number" type="text" id="phoneNumber" name="phoneNumber" className="form-control" />
                                <label htmlFor="name">Phone #</label>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="mb-3">
                                <button className="btn btn-lg btn-primary">Create</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default CustomerForm;
