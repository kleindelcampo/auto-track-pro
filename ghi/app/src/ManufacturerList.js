import React, {useState, useEffect} from 'react';


function ManufacturerList(props) {
    const [manufacturers, setManufacturers] = useState([]);
    async function getManufacturers() {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        if (response.ok) {
            const { manufacturers } = await response.json();
            setManufacturers(manufacturers)
        } else {
            console.error('An error occured while fetching this data');
        }
        ;
    }

    useEffect(()=> {
        getManufacturers();
    }, []);

    if (manufacturers === undefined) {
        return null;
    }

    return (
        <>
        <h1>List of Manufacturers</h1>
        <table className="table table-striped">
          <thead>
            <tr className="table-secondary">
              <th scope="col">Manufacturers</th>
            </tr>
          </thead>
          <tbody>
            { manufacturers.map(manufacturer =>{
                return (
                    <tr key={manufacturer.name}>
                    <td>{manufacturer.name}</td>
            </tr>
                )
            })

            }
          </tbody>
        </table>
        </>
    );
}

export default ManufacturerList;
