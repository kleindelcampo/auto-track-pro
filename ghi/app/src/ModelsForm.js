import React, {useState, useEffect} from 'react';


function ModelsForm(props) {
    const [modelName, setModelName] = useState('');
    const [image, setImage] = useState('');
    const [manufacturers, setManufacturers] = useState([]);
    const [manufacturer, setManufacturer] = useState('');

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleNameChange = (e) => {
        const value = e.target.value;
        setModelName(value);
    }

    const handleImageChange = (e) => {
        const value = e.target.value;
        setImage(value);
    }

    const handleManufacturerChange = (e) => {
        const value = e.target.value;
        setManufacturer(value);
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        const data = {};

        data.name = modelName;
        data.picture_url = image;
        data.manufacturer_id = manufacturer;

        const modelUrl = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application.json',
            },
        };


        const response = await fetch(modelUrl, fetchConfig);

        if (response.ok) {
            const newModel = await response.json();

            setModelName('');
            setImage('');
            setManufacturer('');
        }

    }




    return (
        <div className="container">
            <div className="col">
                <div className="card shadow">
                    <form onSubmit={handleSubmit} id="create-model-form">
                        <h1 className="card-title">Register a new vehicle model</h1>
                        <div className="row">
                            <div className="col">
                                <div className="form-floating mb-3">
                                <input onChange={handleNameChange} value={modelName} required placeholder="Model name" type="text" id="modelName" name="modelName" className="form-control" />
                                <label htmlFor="name">Model name</label>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <div className="form-floating mb-3">
                                <input onChange={handleImageChange} value={image} required placeholder="Picture url" type="url" id="image" name="image" className="form-control" />
                                <label htmlFor="name">Picture URL</label>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <div className="form-floating mb-3">
                                <select onChange={handleManufacturerChange} value={manufacturer} name="manufacturer" id="manufacturer" className="form-select" required>
                                <option value="">Choose a manufacturer</option>
                                {manufacturers.map(manufacturer => {
                                    return (
                                        <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                                        )
                                        })}
                                </select>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="mb-3">
                                <button className="btn btn-lg btn-primary">Register!</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    )
}
export default ModelsForm;
