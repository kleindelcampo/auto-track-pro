import React, {useState, useEffect} from 'react';



function SalesHistory() {
    const [sales, setSales] = useState([]);
    const [filter, setFilter] = useState([]);
    const [searchEId, setSearchEId] = useState('');
    const [submitted, setSubmitted] = useState(false);

    useEffect(() => {
        const fetchSales = async () => {
            const url = 'http://localhost:8090/api/sales/';
            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                setSales(data.sales)
            }

        };

        fetchSales();
    }, []);

    // console.log(sales)

    const handleSearch = async () => {
        const results = sales.filter((sale) =>
            sale.salesperson.employee_id.includes(searchEId)
        );
        setFilter(results);
        setSubmitted(true);
    };


    return (
        <>
        <div className="px-4 py-5 my-1 mt-0 text-center">
            <h1 className="display-5">Sales History</h1>
        </div>
        <div className="row height d-flex justify-content-center align-items-center">
                <div className="col-md-auto">
                    <div className="input-group mb-2">
                        <input
                            type="text"
                            value={searchEId}
                            onChange={(e) => setSearchEId(e.target.value)}
                        />
                        <button
                            onClick={handleSearch}
                            type="button"
                            className="btn btn-outline-secondary"
                        >
                            Search by Employee ID
                        </button>
                    </div>
                </div>
            </div>
            {submitted && filter.length === 0 && (
                <div className="alert alert-danger mb-0 p-4 mt-4" id="danger-message">
                    Sale not found
                </div>
            )}
            {filter.length > 0 && (
                <table className='table table-striped'>
                    <thead>
                        <tr className='table-secondary'>
                            <th>Salesperson Name</th>
                            <th>Customer</th>
                            <th>VIN</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {filter.map((f) => {
                            return(
                            <tr className="table-row" key={f.id}>
                                <td>{f.salesperson.first_name} {f.salesperson.last_name}</td>
                                <td>{f.customer.first_name} {f.customer.last_name}</td>
                                <td>{f.automobile.vin}</td>
                                <td>{f.price}</td>
                            </tr>
                            );
                        })}
                    </tbody>
                </table>
            )}
            {/* <table className='table table-striped'>
                <thead>
                    <tr className='table-secondary'>
                        <th>Salesperson Name</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                {sales.map((sale) =>{
                    return(
                        <tr key={sale.id}>
                            <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                            <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>{sale.price}</td>
                        </tr>
                    )
                })}
                </tbody>
            </table> */}
        </>
    );

}

export default SalesHistory;
