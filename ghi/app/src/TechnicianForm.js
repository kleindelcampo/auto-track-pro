import { useState } from "react";

const TechnicianForm = () => {
	const [first_name, setFirstName] = useState("");
    const [last_name, setLastName] = useState("");
	const [employee_id, setEmployeeID] = useState("");
	const [submitted, setSubmitted] = useState(false);
	const [error, setError] = useState(false);

	const handleSubmit = async (event) => {
		event.preventDefault();

		const data = { first_name, last_name, employee_id};


		const technicianUrl = "http://localhost:8080/api/technicians/";
		const fetchConfig = {
			method: "post",
			body: JSON.stringify(data),
			headers: {
				"Content-Type": "application/json",
			},
		};

		const response = await fetch(technicianUrl, fetchConfig);
		if (response.ok) {
			event.target.reset();
			setFirstName("");
            setLastName("");
			setEmployeeID("");
			setSubmitted(true);
			setError("");
		} else {
			console.error("Invalid employee id");
			setError(true);
		}
	};

	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="px-4 py-5 my-1 mt-0 text-center">
					<h1 className="display-5">Create a Technician</h1>
					<form id="create-technician-form" onSubmit={handleSubmit}>
						<div className="form-floating mb-3">
							<input
								onChange={(e) => setFirstName(e.target.value)}
								placeholder="first-name"
								required
								type="text"
								name="first-name"
								id="first-name"
								className="form-control"
							/>
							<label htmlFor="name">First Name</label>
						</div>
                        <div className="form-floating mb-3">
							<input
								onChange={(e) => setLastName(e.target.value)}
								placeholder="last-name"
								required
								type="text"
								name="last-name"
								id="last-name"
								className="form-control"
							/>
							<label htmlFor="name">Last Name</label>
						</div>
						<div className="form-floating mb-3">
							<input
								onChange={(e) => setEmployeeID(e.target.value)}
								placeholder="employeeID"
								required
								type="employeeID"
								name="employeeID"
								id="employeeID"
								className="form-control"
							/>
							<label htmlFor="employeeID">Employee ID</label>
						</div>
						<div className="col text-center">
							<button className="btn btn-primary">Create</button>
						</div>
					</form>
					{error && (
						<div
							className="alert alert-danger mb-0 p-4 mt-4"
							id="success-message">
							Please choose a different Employee ID (must only be numbers)
						</div>
					)}
					{!error && submitted && (
						<div
							className="alert alert-success mb-0 p-4 mt-4"
							id="success-message">
							Technician successfully created.
						</div>
					)}
				</div>
			</div>
		</div>
	);
};
export default TechnicianForm;
