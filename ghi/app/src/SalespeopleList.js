import React, {useState, useEffect} from 'react';

function SalespeopleList() {
    const [salespeople, setSalespeople] = useState([]);
    async function getSalespeople() {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        if (response.ok) {
            const { salespeople } = await response.json();
            setSalespeople(salespeople)
        } else {
            console.error('An error occured while fetching this data');
        };
    }

    useEffect(()=> {
        getSalespeople();
    }, []);

    if (salespeople === undefined) {
        return null;
    }
    return (
        <>
        <div className="px-4 py-5 my-1 mt-0 text-center">
        <h1 className='display-5'>List of Salespeople</h1>
        <table className="table table-striped">
            <thead>
            <tr className="table-secondary">
                <th>First Name</th>
                <th>Last Name</th>
                <th>Employee ID</th>
            </tr>
            </thead>
            <tbody>
                { salespeople.map(salesperson =>{
                    return (
                        <tr key={salesperson.id}>
                            <td>{salesperson.first_name}</td>
                            <td> {salesperson.last_name}</td>
                            <td>{salesperson.employee_id}</td>
                        </tr>
                    )
                })
                }
            </tbody>
        </table>
        </div>
        </>
    );
}
export default SalespeopleList;
