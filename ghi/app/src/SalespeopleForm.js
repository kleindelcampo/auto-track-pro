import React, {useEffect, useState} from 'react'

function SalespeopleForm() {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [employeeId, setEmployeeID] = useState('');

    const handleFirstNameChange = (e) => {
        const value = e.target.value;
        setFirstName(value);
    }

    const handleLastNameChange = (e) => {
        const value = e.target.value;
        setLastName(value);
    }

    const handleEmployeeIdChange = (e) => {
        const value = e.target.value;
        setEmployeeID(value);
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        const data = {};

        data.first_name = firstName;
        data.last_name = lastName;
        data.employee_id = employeeId;

        const url = 'http://localhost:8090/api/salespeople/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application.json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            const newEmployee = await response.json();

            setFirstName('');
            setLastName('');
            setEmployeeID('');

        } else {
            alert("This employee ID has already been assigned.")

        }


    }


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="px-4 py-5 my-1 mt-0 text-center">
                    <form onSubmit={handleSubmit} id='create-salesperson-form'>
                        <h1 className='display-5'>Create New Salesperson</h1>
                        <div className='row'>
                            <div className='col'>
                                <div className='form-floating mb-3'>
                                    <input onChange={handleFirstNameChange} value={firstName} required placeholder='First name' type='text' id='firstName' name='firstName' className='form-control' />
                                        <label htmlFor='firstName'>First name</label>
                                </div>
                            </div>
                        </div>
                        <div className='row'>
                            <div className='col'>
                                <div className='form-floating mb-3'>
                                    <input onChange={handleLastNameChange} value={lastName} required placeholder='Last name' type='text' id='lastName' name='lastName' className='form-control' />
                                        <label htmlFor='lastName'>Last name</label>
                                </div>
                            </div>
                        </div>
                        <div className='row'>
                            <div className='col'>
                                <div className='form-floating mb-3'>
                                    <input onChange={handleEmployeeIdChange} value={employeeId} required placeholder='Employee ID' type='number' id='employeeId' name='employeeId' className='form-control' />
                                        <label htmlFor='lastName'>Employee ID</label>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="mb-3">
                                <button className="btn btn-lg btn-primary">Create</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
}



export default SalespeopleForm;
