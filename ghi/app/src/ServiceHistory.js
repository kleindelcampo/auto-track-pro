import React, { useState, useEffect } from "react";

const ServiceHistory = () => {
    const [appointments, setAppointments] = useState([]);
    const [filter, setFilter] = useState([]);
    const [searchByVin, setSearchByVin] = useState("");
    const [submitted, setSubmitted] = useState(false);

    useEffect(() => {
        const fetchAppointments = async () => {
            const url = "http://localhost:8080/api/appointments/";
            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                setAppointments(data.appointments);
            }
        };

        fetchAppointments();
    }, []);

    const handleSearch = async () => {
        const results = appointments.filter((appointment) =>
            appointment.vin.includes(searchByVin)
        );
        setFilter(results);
        setSubmitted(true);
    };

    return (
        <>
            <div className="px-4 py-5 my-1 mt-0 text-center">
                <h1 className="display-5">Service History</h1>
            </div>
            <div className="row height d-flex justify-content-center align-items-center">
                <div className="col-md-auto">
                    <div className="input-group mb-2">
                        <input
                            type="text"
                            value={searchByVin}
                            onChange={(e) => setSearchByVin(e.target.value)}
                        />
                        <button
                            onClick={handleSearch}
                            type="button"
                            className="btn btn-outline-secondary"
                        >
                            Search by VIN
                        </button>
                    </div>
                </div>
            </div>
            {submitted && filter.length === 0 && (
                <div className="alert alert-danger mt-4" role="alert">
                <p className="alert-text text-center">Service not found</p>
            </div>
            )}
            {filter.length > 0 && (
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Owner</th>
                            <th>Vin</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                            <th>Finished</th>
                        </tr>
                    </thead>
                    <tbody>
                        {filter.map((filter) => {
                            return (
                                <tr className="table-row" key={filter.id}>
                                    <td>{filter.customer}</td>
                                    <td>{filter.vin}</td>
                                    <td>
                                        {new Date(filter.date_time).toLocaleDateString("en-US")}
                                    </td>
                                    <td>
                                        {new Date(filter.date_time).toLocaleTimeString([], {
                                            hour: "2-digit",
                                            minute: "2-digit",
                                        })}
                                    </td>
                                    <td>{filter.technician.first_name} {filter.technician.last_name} </td>
                                    <td>{filter.reason}</td>
                                    <td>{filter.finished ? "Yes" : "No"} </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            )}
            <div className="custom-header text-center px-4 py-5 my-1 mt-0">
                <h1 className="display-5">Service Appointments</h1>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Vin</th>
                        <th>Owner</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Vip</th>
						<th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map((appointment) => {
                        if (!appointment.is_finished) {
                            return (
                                <tr className="table-row" key={appointment.id}>
                                    <td>{appointment.vin}</td>
                                    <td>{appointment.customer}</td>
                                    <td>
                                        {new Date(appointment.date_time).toLocaleDateString(
                                            "en-US"
                                        )}
                                    </td>
                                    <td>
                                        {new Date(appointment.date_time).toLocaleTimeString([], {
                                            hour: "2-digit",
                                            minute: "2-digit",
                                        })}
                                    </td>
                                    <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                                    <td>{appointment.reason}</td>
                                    <td>{appointment.vip ? "Yes" : "No"}</td>
									<td>{appointment.status}</td>
                                </tr>
                            );
                        }
                        return null;
                    })}
                </tbody>
            </table>
        </>
    );
};

export default ServiceHistory;
